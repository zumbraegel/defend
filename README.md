# DEFEND

Verifiable Delay Functions from Endomorphism Rings

Knud Ahrens . Jens Zumbrägel

Sage Code


## About

This project implements a verifiable delay function based on isogenies
of supersingular elliptic curves.  It uses Deuring correspondence and
computation of endomorphism rings for the delay.  In our approach the
input is a path in the 2-isogeny graph and the output is the maximal
order isomorphic to the endomorphism ring of the curve at the end of
that path.

For more details see [eprint](https://eprint.iacr.org/2023/1537)


## History

7 Oct 2023: Initial Version
