#VDF Example
#This would work as a VDF for proper parameters and optimisation.

#load functions
attach(setup.sage) #provides public functions
attach(ellipticCurves.sage) #includes internal functions for elliptic curves
attach(lattices.sage) #includes internal functions for lattices

#setup #provide/set public parameters pp = (p,E1,O1,n)
p = 103 #prime p governs security
n = 7 #Fix the number of steps in the 2-isogeny graph. This sets the delay.
E1,O1 = setup(p)

#challenge #provide challenge x = (S,j)
S,j = challenge(E1,n)

#evaluate #on input (pp,x) provide response (y,pi) with y = O and pi = E
O,E = evaluate(E1,O1,S) #not fully implemented yet (rightOrder still needs some work)

#verify #given (pp, x = (S,j), y = O, pi = E) verify that E is in the correct isomorphism class and O is unique representation of order isomorphic to End E
verify(p,j,O,E)


#Toy Example 
#This is a toy example to show the general idea.
#This does not work as a VDF and is rather for debugging.
#For a VDF take the VDF example.

#load functions
attach(setup.sage) #provides public functions
attach(ellipticCurves.sage) #includes internal functions for elliptic curves
attach(lattices.sage) #includes internal functions for lattices 

#setup #provide/set public parameters pp = (p,D,ext,O0,B,M)
p = 103 #prime p governs security
D = p + 1 #degree of isogeny, influences delay
ext = 1 #extension degree such that E[D] is rational over Fp(2*ext) #buggy for ext > 1
O0,B,M = setup0(p,D,ext)

#challenge #provide challenge x = (P,E)
P,E = challenge0(D,B)

#evaluate #on input (pp,x) provide response (y,pi) with y = O and pi empty
O = evaluate0(D,O0,M,P,E) #not much delay due to massive pre-computation #not fully implemented yet (rightOrder still needs some work)

#verify #given (pp, x = (P,E), y = O, pi) verify that O is unique representation of order isomorphic to End E
verify0(E,O)
