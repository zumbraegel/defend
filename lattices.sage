#functions for lattices written as 4x4 matrices

#column-style Hermite normal form for half-integer square matrices
def hnf (M): #M half-integer (square) matrix
    H = (2 * M).change_ring(ZZ); #convert to integer matrix to apply HNF
    H = H.transpose()[::-1,::-1].hermite_form(include_zero_rows=False).transpose()[::-1,::-1]; #flip to apply row-style hnf and flip back
    H = (1/2) * H; #retrieve HNF of original matrix
    return H
    
#union of a list of lattices
def union (L): #input: list of matrices
    C = hnf(concatinate(L));
    return C

#concatinate matrices
def concatinate (L): #L list of matrices
    C = block_matrix(L,nrows=1);
    return C

#matrix corresponding to multiplication from the right for elements of the quaternion algebra
def rightMult (c,p): #c (column) vector, p prime
    R1 = matrix.identity(4);
    Ri = matrix(4,[0,-1,0,0, 1,0,0,0, 0,0,0,1, 0,0,-1,0]);
    Rj = matrix(4,[0,0,-p,0, 0,0,0,-p, 1,0,0,0, 0,1,0,0]);
    Rk = matrix(4,[0,0,0,-p, 0,0,p,0, 0,-1,0,0, 1,0,0,0]);
    Rc = c[0] * R1 + c[1] * Ri + c[2] * Rj + c[3] * Rk;
    return Rc

#compute lattice for I = O0<alpha,N> with alpha = a + b * theta0 - i (encoded as (a,b)) #for E0 only
def idealToLattice0 (alpha,N,O): #alpha = (a,b), N norm of ideal, O basis matrix of order
    a,b = alpha;
    c = [a+b/2, -1, b, b/2];
    Rc = rightMult(c,p);
    RN = N*matrix.identity(4);
    L = union([Rc*O,RN*O]);
    return L

#compute lattice for I = O0<alpha,N> with alpha = a + b * theta - i (encoded as (a,b)) 
def idealToLattice (a,b,ct,O): #a,b integers, ct column vector representing theta, O basis matrix of order
    c = [a+b*ct[0], -1+b*ct[1], b*ct[2], b*ct[3]]; #hard-coded theta, might have to change if not orthogonal to eta = i
    Rc = rightMult(c,p);
    R2 = 2*matrix.identity(4);
    L = union([Rc*O,R2*O]);
    return L

#compute the right order for an ideal given as lattice L #work in progress
def rightOrder (L,p): #L lattice, p prime
    R = [rightMult(c,p) for c in matrix.identity(4).columns()]
    A = [hnf(r * L) for r in R];
    Linv = L.inverse()
    B = [hnf(Linv * a) for a in A]
    num = det(L).numerator()
    mapMatrix = matrix(16,4,[B[k][i,j] for i in range(4) for j in range(4) for k in range(4)])
    return findKernelMod(mapMatrix,num) #does not quite work

#find kernel modulo num
def findKernelMod (M,num): #M (half-integer) matrix, num integer
    if num == 1: return M.transpose().integer_kernel().basis_matrix().transpose() #replace ".transpose().integer_kernel()" by ".right_kernel()"?
    S,Tl,Tr = M.change_ring(ZZ).smith_form() #allow QQ here and enforce ZZ later?
    rows,cols = S.dimensions()
    m = min(rows,cols)
    K = matrix(ZZ,cols,0) #find kernel of S
    for j in range(m):
        d = ZZ(gcd(S[j,j],Zmod(num)(0)))
        if d != 1:
            if d == 0: d = num
            aug = matrix(ZZ,cols,1); aug[j]= ZZ(num/d)
            K = K.augment(aug)
    KM = Tr * K.change_ring(Zmod(num))
    C = (KM).columns()
    rk,ck = K.dimensions()
    for j in range(ck):
        for k in range(rk):
            d = ZZ(gcd(C[j][k],0)) 
            if d == 1:
                C[j] = (C[j]/C[j][k]).change_ring(ZZ) #if column contains a unit, make that unit 1
                break
    return hnf(matrix(ZZ,ck,rk,C).transpose())
