#functions for supersingular elliptic curves

#[i]: (x,y) -> (-x, i*y)
def multI (P): #P point #sage is sometimes buggy with extensions
    E = P.curve(); F = E.base_field()
    i = F.primitive_element()^((F.order()-1)/4) #does this always work?
    assert i.multiplicative_order() == 4
    Q = E(-P[0],i*P[1])
    return Q
	
#Frobenius map: (x,y) -> (x^p, y^p)	
def phi (P): #P point
    E = P.curve()
    Q = E(P[0]^p,P[1]^p)
    return Q

#j + (1+k)/2 #for E0 only
def theta0 (P): #P point
    E = P.curve()
    return phi(P) + E(half(P) + half(multI(phi(P))))

#(O[2]-O[1]*O[3]) + O[3] #<P,theta(P)> = E[D] for D = P.order()? #test if theta and O[1] map E[2] to E[2] #is this orthogonal to O[1]?
def theta (P,O): #P point, O basis matrix of order
    E = P.curve()
    #compute O[1]*O[3]
    c2 = [O[0,1]*O[0,3]-O[1,1]*O[1,3]-p*O[2,1]*O[2,3]-p*O[3,1]*O[3,3], \
          O[0,1]*O[1,3]+O[1,1]*O[0,3]+p*O[2,1]*O[3,3]-p*O[3,1]*O[2,3], \
          O[0,1]*O[2,3]-O[1,1]*O[3,3]+O[2,1]*O[0,3]+O[3,1]*O[1,3], \
          O[0,1]*O[3,3]+O[1,1]*O[2,3]-O[2,1]*O[1,3]+O[3,1]*O[0,3]]
    c = [O[i,2] - c2[i] + O[i,3] for i in range(4)] #result of (O[2]-O[1]*O[3]) + O[3]
    Q = E(evalColumn(c,P))
    return Q,c

#find point Q such that 2*Q = P
def half (P):
    DivPoints = P.division_points(2)
    if (not DivPoints):
        E = P.curve(); F = E.base_field()
        assert F.degree() < 12
        Q = E.change_ring(F.extension(2))(P) #go to extension if necessary
        DivPoints = Q.division_points(2)
    return DivPoints[0]

#find points P,Q such that <P,Q> = E/Fp2 #replace by real basis?
def basis (E): #E supersingular elliptic curve
    assert E.is_supersingular()
    sqrtOrd = sqrt(E.order())
    P,Q = E.gens()
    assert P.order() == sqrtOrd
    assert Q.order() == sqrtOrd 
    return P,Q

#decompose point P wrt basis [R,S]
def XDLOG (P,R,S): #P,R,S points
    A = R.order(); B = S.order(); logR = {}
    for a in range(A): logR[a*R] = a #LUT for DLOG wrt R
    for b in range(B):
        Q = P-b*S
        if logR.get(Q): return logR[Q], b
    print("Can not decompose")

#find (minimal) extension degree such that E[D] is rational (better: fix extension degree and use findDegree to get suitable D)
def findExtension (p,D): #p prime, D integer
    for k in range(1,12): #sage is sometimes buggy with coercions for extensions of degree larger than 11
        if (p^k - (-1)^k) % D == 0: #a supersingular elliptic curve with (p + 1)^2 points over Fp2 has (p^k - (-1)^k)^2 points over Fp2k
            return k
    print("Extension degree > 11")

#better than findExtension
def findDegree (p,ext,options): #prime p, extension degree ext, options: 0 = max degree, 1 (else) =  possible factors
    if ext > 11:
        print("This sage implemantation can't handle extensions of degree larger than 11.")
    assert ext < 12
    order = p^ext - (-1)^ext
    if ext > 5:
        D = order / 2 #prevents extensions during half(P)
    else: D = order
    if not options:
        return D
    else:
        return factor(D)

#computes a basis for E[D] and the action of theta and multI on this basis as matrix #for E0 only #might be buggy for extensions
def precomputeBasis0 (E,D,ext): #E supersingular elliptic curve, D integer, ext extension degree
    F = E.base_field()
    assert F.degree() == 2
    if ext == 1:
        Fext = F; Eext = E
    else:
        Fext = F.extension(ext)
        Eext = E.change_ring(Fext)
    PE,QE = basis(Eext)
    d = ZZ((p^ext - (-1)^ext)/D) #(p^ext - (-1)^ext) = PE.order() = QE.order()
    PD = d * PE; QD = d * QE #basis of E[D]
    assert PD.order() == D
    assert QD.order() == D
    assert PD != QD
    tPD = theta0(PD); tQD = theta0(QD)
    iPD = multI(PD); iQD = multI(QD)
    #return PD,QD,tPD,tQD,iPD,iQD
    #matrix form
    tPDv = XDLOG(tPD,PD,QD); tQDv = XDLOG(tQD,PD,QD)
    iPDv = XDLOG(iPD,PD,QD); iQDv = XDLOG(iQD,PD,QD)
    Mt = matrix(2, tPDv+tQDv).transpose() #transformation matrix of theta0 on E[D]
    Mi = matrix(2, iPDv+iQDv).transpose() #transformation matrix of multI on E[D]
    return PD,QD,Mt,Mi

#find ideal corresponding to isogeny with kernel <P>, #for E0 only
def pointToIdeal0 (P,D,precomp): #P = (a,b) represents point P = a*PD + b*QD of order D, precomp = [Mt,Mi] transformation matrices of theta0,multI, respectively
    R = Zmod(D)
    Mt,Mi = precomp
    v = vector(ZZ,[P[0],P[1]]); w = Mt * v
    M = matrix(ZZ,2,[v]+[w]).transpose()
    Minv = M.inverse()
    r = (Minv*Mi*v).change_ring(R).change_ring(ZZ)
    return r[0],r[1] #I = O<alpha,D> with alpha = a + b*(j + (1+k)/2) - i (encoded as (r[0],r[1]))

#find ideal corresponding to isogeny with kernel <P>
def pointToIdeal (P,O):
    E = P.curve()
    tP,ct = theta(P,O)
    C = O.columns()
    iP = E(evalColumn(C[1],P)) #are theta and C[1] orthogonal?
    a,b = XDLOG(iP,P,tP)
    return a,b,ct

#evaluate the isogeny corresponding to the element v0 + v1*i + v3*j + v4*k
def evalColumn (v,P): #v in 1/2*ZZ^4, P point
    w = 2*vector(QQ,v) #can cause problems for P in E[2]
    assert all(w[i].is_integral() for i in range(len(w)))
    w = w.change_ring(ZZ)
    Q = w[0]*P + w[1]*multI(P) + w[2]*phi(P) +w[3]*multI(phi(P))
    return half(Q)

#hash function from "Verifiable isogeny walks: Towards an isogeny-based postquantum VDF"
def hashF (S,F): #S binary string, F (finite) field
    j_previous = F(1728)
    j_current = F(287496)
    for s in S: 
        a = F(j_current^2 -1488 * j_current - j_previous + 162000)
        b = sqrt(F(j_current^4 - 2976 * j_current^3 + 2 * j_current^2 * j_previous + 2532192 * j_current^2 -2976 * j_current * j_previous - 645205500 * j_current - 3 * j_previous^2 + 324000 * j_previous - 8748000000)) #implement other sqrt algorithm? (this one might vary over different systems)
        j_previous = j_current
        j_current = (a + (1 - 2 * s) * b)/2
    return j_current
