#public functions for VDF example

#setup for VDF example
def setup (p): #p prime
    assert p % 4 == 3
    Fp2.<i> = GF(p^2,modulus=x^2+1)
    E0 = EllipticCurve(Fp2,[1,0])
    O0 = matrix(4,[1,0,0,1/2, 0,1,1/2,0, 0,0,1/2,0, 0,0,0,1/2])
    P0 = E0(i,0) #or P0 = E0(-i,0) for E1: y^2 = x^3 + 11x - 14i
    E1 = E0.isogeny_codomain(P0) #y^2 = x^3 + 11x + 14i with j-invariant 287496 (start of hash function)
    #compute O1 order corresponding to endomorphism ring of E1
    alpha = XDLOG(multI(P0),P0,theta0(P0)) #I = O<alpha,2> with alpha = a + b*(j + (1+k)/2) - i (encoded as (a,b))
    L = idealToLattice0(alpha,2,O0) 
    O1 = rightOrder(L,p) #not fully implemented yet (rightOrder still needs some work)
    return E1,O1

#create a challenge for VDF example
def challenge (E1,n): #E1 start curve of hashF (fixed), n length of path 
    S = [randrange(2) for i in range(n)] #random binary string of length n
    j = hashF(S,E1.base_field()) #find j-invariant at end of path
    return S,j

#evaluation of VDF example
def evaluate (E1,O1,S): #E1 start curve of hashF (fixed), O1 basis matrix of max order, S binary string
    F = E1.base_field(); p = F.characteristic()
    E = E1; O = O1
    P0 = E(0); T2 = P0.division_points(2) #P0 is point (0 : 1 : 0), T2 is 2-torsion E[2]
    assert len(T2) == 4 #check that there are 4 points in E[2]
    j_previous = F(1728) #j-invariant of E0
    j_current = F(287496) #j-invariant of E1
    for s in S:
        a = F(j_current^2 - 1488 * j_current - j_previous + 162000)
        b = sqrt(F(j_current^4 - 2976 * j_current^3 + 2 * j_current^2 * j_previous + 2532192 * j_current^2 - 2976 * j_current * j_previous - 645205500 * j_current - 3 * j_previous^2 + 324000 * j_previous - 8748000000)) #implement other sqrt algorithm? (this one might vary over different systems)
        j_previous = j_current
        j_current = (a + (1 - 2 * s) * b)/2
        for P in T2: #can be done in parallel for more precise delay estimation
            Enext = E.isogeny_codomain(P)
            if Enext.j_invariant() == j_current: #check which of points in E[2] is the kernel for the isogeny to the next j-invariant
                Pker = P
                E = Enext
                break
        else:
            print("No isogeny found")
            return False
        P0 = E(0); T2 = P0.division_points(2)
        assert len(T2) == 4
        a,b,ct = pointToIdeal(Pker,O)
        L = idealToLattice(a,b,ct,O)
        O = rightOrder(L,p) #not fully implemented yet (rightOrder still needs some work)
    return O,E

#verification for VDF example
def verify (p,j,O,E): #p prime,j j-invariant, O matrix basis of max order, E elliptic curve
    if E.j_invariant() != j:
        print("Curve has wrong j-invariant")
        return False
    if E.order() != (p + 1)^2:
        print("Curve has wrong order")
        return False
    if O != hnf(O):
        print("Not in Hermite normal form")
        return False
    for i in range(4):
        for j in range(4):
            if O[i,j].denominator() > 2:
                print("Fraction other than 1/2 in order")
                return False
    p = E.base_field().characteristic()
    QA.<ii,jj,kk> = QuaternionAlgebra(-1, -p)
    QO = QA.quaternion_order([O[0,i]+O[1,i]*ii+O[2,i]*jj+O[3,i]*kk for i in range(4)]) #TODO test if ring to prevent error
    if (QO.discriminant() != p):
        print("Order not maximal")
        return False
    B = basis(E)
    C = O.columns()
    for c in C:
        for P in B:
            Q = evalColumn(c,P) #TODO throws an error if not on curve
            x,y = Q.xy()
            if (not E.is_on_curve(x,y)):
                print("Not an endomorphism")
                return False
            if (phi(phi(Q)) != Q):
                print("Not an endomorphism of E/Fp2")
                return False
    return True


#public functions for toy example
#This does not work as a VDF and is rather for debugging.
#For a VDF take the VDF example.

#setup for toy example
def setup0 (p,D,ext): #p prime, D degree of challenge isogeny, ext extension degree such that E0[D] is rational (still buggy for ext > 1)
    assert p % 4 == 3
    Fp2.<i> = GF(p^2,modulus=x^2+1)
    E0 = EllipticCurve(Fp2,[1,0])
    O0 = matrix(4,[1,0,0,1/2, 0,1,1/2,0, 0,0,1/2,0, 0,0,0,1/2])
    PD,QD,Mt,Mi = precomputeBasis0(E0,D,ext)
    B = [PD,QD]; M = [Mt,Mi]
    return O0,B,M

#create a challenge for toy example
def challenge0 (D,B): #D order of isogeny, B basis of E0[D]
    a = randrange(D); b = randrange(D)
    while gcd([a,b,D]) != 1:
        a = randrange(D); b = randrange(D)
    R = B[0]; S = B[1]; P = a*R + b*S #P is point of order D
    assert P.order() == D
    E = B[0].curve().isogeny_codomain(P)
    #jE = E.j_invariant()
    return ((a,b),E)

#evaluation of toy example
def evaluate0 (D,O0,M,P,E): #D order of P, O0 basis matrix of max order, M = [Mt,Mi] transformation matrices of theta0 and multI, respectively, P = (a,b) represents point P = a*B[0] + b*B[1], E final curve
    alpha = pointToIdeal0(P,D,M) #I = O<alpha,2> with alpha = a + b*(j + (1+k)/2) - i (encoded as (a,b))
    L = idealToLattice0(alpha,D,O0)
    p = E.base_field().characteristic()
    O = rightOrder(L,p) #not fully implemented yet (rightOrder still needs some work)
    return O

#verification for toy example
def verify0 (E,O): #E elliptic curve, O matrix basis of max order
    if O != hnf(O):
        print("Not in Hermite normal form")
        return False
    for i in range(4):
        for j in range(4):
            if O[i,j].denominator() > 2:
                print("Fraction other than 1/2 in order")
                return False
    p = E.base_field().characteristic()
    QA.<ii,jj,kk> = QuaternionAlgebra(-1, -p)
    QO = QA.quaternion_order([O[0,i]+O[1,i]*ii+O[2,i]*jj+O[3,i]*kk for i in range(4)]) #TODO test if ring to prevent error?
    if (QO.discriminant() != p):
        print("Order not maximal")
        return False
    B = basis(E)
    C = O.columns()
    for c in C:
        for P in B:
            Q = evalColumn(c,P) #TODO throws an error if not on curve
            x,y = Q.xy()
            if (not E.is_on_curve(x,y)):
                print("Not an endomorphism")
                return False
            if (phi(phi(Q)) != Q):
                print("Not an endomorphism of E/Fp2")
                return False
    return True
